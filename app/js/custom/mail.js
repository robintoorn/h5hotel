 function mail (){
    let firstname = document.getElementById('firstname').value;
    let phone = document.getElementById('phone').value;
    let email = document.getElementById('email').value;
    let message = document.getElementById('message').value;

    if(firstname.length > 0 && phone.length > 0 && email.length > 0 && message.length > 0){
        post('https://mailservice.planetdev.se:8433/api/planet-mail/email-sender-v1', {
            title: 'Ny kontakt!',
            message: 'För -och efternamn: ' + firstname + "\nTelefonnummer: " + phone + "\nE-postadress: " + email + "\nMeddelande:\n\n" + message,
            to: 'njesper98@gmail.com'
        });
    } else {
        let senduhhh = document.getElementById("senduhh");
        senduhhh.style.display = "block";
        senduhhh.innerHTML = "Fyll i alla fält som behövs!";
        setTimeout(function(){ senduhhh.style.display = "none" }, 3000);
    }
};

function post(url, data) {
    return fetch(url, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        body: JSON.stringify(data),
        method: 'POST',
    });
};